﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {
	public GameObject explosion;
	public GameObject playerExplosion;
	private GameController gameController;
	public int scoreValue;

	void Start() {
		GameObject gameControllerObj = GameObject.FindWithTag ("GameController");
		if (gameControllerObj != null) {
			gameController = gameControllerObj.GetComponent<GameController>();
		}

		if (gameController == null) {
			Debug.Log("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter(Collider other) {
		if (!other.gameObject.CompareTag ("Boundary")) {
			if (other.gameObject.CompareTag("Player")) {
				Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
				gameController.GameOver();
			}

			Instantiate(explosion, transform.position, transform.rotation);
			Destroy (other.gameObject);
			Destroy (gameObject);
			gameController.AddScore(scoreValue);
		}
	}
}
