﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {
	public GameObject hazard;
	public Vector3 spawnValue;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public Text scoreText;
	private int score;
	public Text restartText;
	public Text gameOverText;
	private bool restart;
	private bool gameOver;

	void Start() {
		restart = false;
		gameOver = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
	}

	void Update() {
		if (restart && Input.GetKeyDown (KeyCode.R)) {
			Application.LoadLevel(Application.loadedLevel);
		}
	}

	// Use this for initialization
	IEnumerator SpawnWaves() {
		yield return new WaitForSeconds(startWait);
		while(!gameOver) {
			for (int i = 0;i < hazardCount; i++) {
				float xSpawn = Random.Range (-spawnValue.x, spawnValue.x);
				Vector3 spawnPosition = new Vector3 (xSpawn, spawnValue.y, spawnValue.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				
				if (gameOver) {
					restart = true;
					restartText.text = "Press 'R' for restart";
					break;
				}

				yield return new WaitForSeconds(spawnWait);
			}

			yield return new WaitForSeconds(waveWait);
		}


	}

	void UpdateScore() {
		scoreText.text = "Score: " + score;
	}

	public void AddScore(int scoreValue) {
		score += scoreValue;
		UpdateScore ();
	}

	public void GameOver() {
		gameOver = true;
		gameOverText.text = "GAME OVER!";
	}
}
